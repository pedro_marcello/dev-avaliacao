﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Data;
using Npgsql;
using WebMarcacao.Model;

namespace WebMarcacao.Repository
{

    public class MedicoRepository
    {
        private string connectionString;
        public MedicoRepository(IConfiguration configuration)
        {
            connectionString = configuration.GetValue<string>("DBInfo:ConnectionString");
        }
        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public void Add(medico item)
        {
            using (NpgsqlConnection dbConnection = new NpgsqlConnection(connectionString))
            {
                item.id = 1;
                item.nome = "Paulo";
                dbConnection.Open();
                dbConnection.Execute(@"INSERT INTO public.""Medico"" (""Id"", ""Nome"") VALUES(@Id,@Nome)", item);
            }

        }
    }
}
