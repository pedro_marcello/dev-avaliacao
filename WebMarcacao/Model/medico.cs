﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebMarcacao.Model
{
    [Table("medico")]

    public class medico
    {
        [Key]
        public int id { get; set; }
        public string nome { get; set; }

    }
}
