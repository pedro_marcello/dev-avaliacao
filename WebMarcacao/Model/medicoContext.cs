﻿using Microsoft.EntityFrameworkCore;


namespace WebMarcacao.Model
{
    public class medicoContext : DbContext
    {
        public DbSet<medico> Medicos { get; set; }
        public medicoContext(DbContextOptions<medicoContext> options) :
    base(options)
        {
        }

    }
}
