﻿using Microsoft.EntityFrameworkCore;


namespace WebMarcacao.Model
{
    public class pacienteContext : DbContext
    {
        public DbSet<paciente> Pacientes { get; set; }
        public pacienteContext(DbContextOptions<pacienteContext> options) :
    base(options)
        {
        }

    }
}
