﻿using Microsoft.EntityFrameworkCore;


namespace WebMarcacao.Model
{
    public class consultaContext : DbContext
    {
        public DbSet<consulta> Consultas { get; set; }
        public consultaContext(DbContextOptions<consultaContext> options) :
    base(options)
        {
        }

    }
}
