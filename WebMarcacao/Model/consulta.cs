﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebMarcacao.Model
{
    [Table("consulta")]
    public class consulta
    {
        [Key]
        public int id { get; set; }
        public DateTime data { get; set; }
        public int id_medico { get; set; }
        public int Id_paciente { get; set; }
    }
}
